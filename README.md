# zrkrlc.com

## About

Imagine if you could work on something for a thousand years. Aside from not appearing in mirrors, I hope you can get a sense of awe from considering such a thing. We stand slackjawed in the face of big monuments, but a monument can be big not only in space but in time as well. 

This site is a modest attempt at Long Now content, a category that appears in the edifice of Gwern Branwen ([gwern.net](https://gwern.net)). I hope it doesn’t disappoint.

## Technical details

To build, you have to use the `lts-12.26` resolver until Stackage updates their listings. So before anything, do:

```
stack install hakyll --resolver=lts-12.26
stack init --resolver=lts-12.26
```

Future build runs can be done using the `build.sh` script in the site’s working directory.