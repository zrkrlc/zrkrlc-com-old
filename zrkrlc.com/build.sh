#!/bin/bash
set -e
stack exec site clean
stack build
stack exec site watch --verbose