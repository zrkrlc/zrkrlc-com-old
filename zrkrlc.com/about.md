---
title: About
---
### Me

I'm **zrkrlc** (pronounced ˈzɜːkəlɒk (in British IPA) or 'zer-ker-lock' in approximate English). I don't do stuff for a living yet (fortunately) so I write about whatever tickles my pickle. And yes, that sounds as wrong as it reads so don't say it out loud.

My life's accomplishments include:

* 12 000+ hours of Internet use since 2007
* turned eight without burning the house down
* learned how to wiggle both my eyebrows
* bought 36 jars of made-in-the-mountains strawberry jam on a single trip
* got lost in a citadel for over two hours when I was in second grade

I also write code in Haskell, Python, and C# to varying degrees of proficiency.

### This Website

I laid some prohibitions for myself in my first post, [Things I will try NOT to do](https://zrkrlc.wordpress.com/2016/03/20/things-i-will-try-not-to-do/).

I will try to write as much as my time would allow. Being unemployed is a difficult commitment to keep so I try to let off steam here as much as I have to. Oh, and don't worry: I don't plan on inundating you with funsies all the time. Sometimes things have to get licking-your-face serious and I hope that doesn't turn you off.

### The Header Image

Pictured at the beginning of every page on this site is the topologist's sine curve, so named because it's a textbook example of why pedestrian's don't bother with such abstract nonsense. It's the graph of the function $f:(0, 1] \rightarrow \mathbb{R}$ plus the origin $(0, 0)$ defined by $f(x) = sin(1/x)$. If we define the following notions:

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Topological_space_examples.svg/360px-Topological_space_examples.svg.png)

*Fig. 1: Examples and non-examples of topological spaces. Note that these are sets. Illustration taken from Wikipedia[1](#fn1)..*

DEF: (topological space)

: a set $X$ of points $x$ together with a function $\mathbf{N}: X \rightarrow 2^{X} \setminus \emptyset$ whose elements we shall call the neighborhoods of $x$, such that:

* $x \in N$ whenever $N \in \mathbf{N}(x)$
* If $N_1$ and $N_2$ are $\in \mathbf{N}(x)$, then $N_1 \cap N_2$ is also $\in \mathbf{N}(x)$.
* If $N \subseteq X$ and $\exists S \in \mathbf{N}(x)$ such that $S \subseteq N$, then $N \in \mathbf{N}(x)$.
* $(\forall N \in \mathbf{N}(x))(\exists M \subseteq N)(\forall a \in N)(M \in \mathbf{N}(a))$

DEF: (connectedness)

: a topological space is connected if it is inexpressible as a union of disjoint nonempty open sets

DEF: (neighborhood system)

: the set of all neighborhoods of a point

DEF: (local connectedness)

: a topological space is locally connected if there exists a neighborhood system for all points which are composed entirely of open, connected sets

DEF: (path)

: a path $f: [0, 1] \rightarrow X$ from a point x to a point y in a topological space X is a continuous function such that $f(0) = x$ and $f(1) = y$

DEF: (pathwise connectedness)

: a topological space X is pathwise connected if there exists a path between any two points in X

Then it can be shown that f is connected but neither locally connected nor pathwise connected. I like maths.

This also brings us to the title.

### The Title

![img](https://upload.wikimedia.org/wikipedia/commons/4/4a/Homotopy.gif)

*Fig. 2: A homotopy. GIF taken from Jim.belk[2](#fn2).*

DEF: (homotopy)

: Let $X$ and $Y$ be topological spaces and f and g mappings between them. A function $H: X \times [0, 1] \rightarrow Y$ is a homotopy if $(x \in X) \implies (H(x, 0) = f(x) \land H(x, 1) = g(x))$.

Which basically means, if you pick up a path, beat it with a sledgehammer (without breaking it) and put it down again, then you just did a homotopy. Did I tell you I like maths?