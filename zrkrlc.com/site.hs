--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll

-- Resolve apostrophe issues
import qualified GHC.IO.Encoding as E

--------------------------------------------------------------------------------
main :: IO ()
main = do
    E.setLocaleEncoding E.utf8
    hakyllWith config $ do
      match "images/*" $ do
          route   idRoute
          compile copyFileCompiler

      match "css/*" $ do
          route   idRoute
          compile compressCssCompiler
          
      -- copies JS scripts (among other things)
      match "src/*" $ do
          route   idRoute
          compile copyFileCompiler

      match (fromList ["about.md", "contact.markdown"]) $ do
          route   $ setExtension "html"
          compile $ pandocCompiler
              >>= loadAndApplyTemplate "templates/default.html" defaultContext
              >>= relativizeUrls
              
      -- Builds tag lists
      -- TODO: have tags link to their tag pages
      tags <- buildTags "posts/*" (fromCapture "tags/*.html")
      tagsRules tags $ \tag pattern -> do
          let title = "Tag: \"" ++ tag ++ "\""
          route $ idRoute
          compile $ do
              posts <- recentFirst =<< loadAll pattern
              let ctx = constField "title" title <> listField "posts" (postCtxWithTags tags) (return posts) <> defaultContext
                
              makeItem ""
                  >>= loadAndApplyTemplate "templates/tag.html" ctx
                  >>= loadAndApplyTemplate "templates/default.html" ctx
                  >>= relativizeUrls
                         
      match "posts/*" $ do
          route $ setExtension "html"
          compile $ do
              pandocCompiler
                  >>= loadAndApplyTemplate "templates/post.html" (postCtxWithTags tags)
                  >>= loadAndApplyTemplate "templates/default.html" postCtx
                  >>= relativizeUrls

      create ["archive.html"] $ do
          route idRoute
          compile $ do
              posts <- recentFirst =<< loadAll "posts/*"
              let archiveCtx =
                      listField "posts" postCtx (return posts) <>
                      constField "title" "Archives"            <>
                      defaultContext

              makeItem ""
                  >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                  >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                  >>= relativizeUrls


      match "index.html" $ do
          route idRoute
          compile $ do
              posts <- recentFirst =<< loadAll "posts/*"
              let indexCtx =
                      listField "posts" postCtx (return posts) <>
                      constField "title" "Home"                <>
                      defaultContext

              getResourceBody
                  >>= applyAsTemplate indexCtx
                  >>= loadAndApplyTemplate "templates/default.html" indexCtx
                  >>= relativizeUrls

      match "templates/*" $ compile templateBodyCompiler
    


--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%Y %B %e" <>
    defaultContext
    
-- Builds context for tag pages generation
postCtxWithTags :: Tags -> Context String
postCtxWithTags tags = tagsField "tags" tags <> postCtx
    
config :: Configuration
config = defaultConfiguration
  { destinationDirectory = "_site"
  , previewPort          = 8000
  }
