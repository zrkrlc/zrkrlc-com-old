---
title: The four intuitions
status: lots of introspection and typical-minding; caveat lector
confidence: possible
tags: mathematics
---

## I.

Are there other ways to think mathematically than through logic and proof? This is one of the questions answered by Barry Mazur ten years ago when he wrote _[“What should a professional mathematician know?“](http://www.math.harvard.edu/~mazur/preprints/math_ed_2.pdf)_ A mathematician with rather profound proclivities, he is known among other things for the evocatively named **Mazur swindle**, which is most easily understood with an example, i.e., “proving” that 1 = 0:

![](https://i.imgur.com/xLTVhJn.png)

<small>Fig. 1: Let’s unpack this. The first equals sign tells you that adding zero to a number doesn’t change anything, and so is adding an infinite number of them. The second and third equals signs tells you that you can group addition and subtraction however you want and still get the same result (e.g., (1 + 2) + 3 = 1 + (2 + 3); i.e., that addition is associative). The last equals sign is, well, 0 + 0 + … = 0. The point here is that you can get a lot of amazing wrong things done under those ellipses (…) and only a more careful treatment of algebra will save you.</small>

$A + B = \frac{C}{D}$

Turns out there are instances in mathematics where this kind of joke “proof” works. A good example of this would be in knot theory. Mathematicians usually single out the simplest possible closed loop of rope as the _trivial_ knot, shaped ‘O’. An example of a nontrivial knot on the other hand is the trefoil:

![](https://upload.wikimedia.org/wikipedia/commons/a/ac/Blue_Trefoil_Knot_Animated.gif)

<small>Fig. 2: Let that animated goodness sink in.</small>

Now, two knots are said to be _equivalent_ if you can move the rope around without breaking it to make the knots resemble each other, and this notion of equivalence gives us a way of classifying the different kinds of knots possible: just group together equivalent knots.

What would happen if I wanted to combine knots together? Let A and B represent knots. Then Wikipedia gives us a clear demonstration of A + B (called knot sum):

![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Sum_of_knots.png/1024px-Sum_of_knots.png)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Sum_of_knots2.png/1024px-Sum_of_knots2.png)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Sum_of_knots3.svg/1024px-Sum_of_knots3.svg.png)

<small>Fig. 3: I’m sweeping a number of details under the rug here, but this works as a guide to intuition. In particular, a knot sum has to respect certain properties for the it to be well-defined.</small>

The Mazur swindle then tells us that, in three dimensions at least, you can never get the trivial knot from a sum of two nontrivial knots. Assuming A + B is trivial, then:

![](https://i.imgur.com/V7CdzoO.png)

so A must be trivial (and B by a similar argument). 

If you’re a nonmathematician like me, how do you discover such things? Is it even possible to discover such things on your own? What does it mean to ‘discover’ mathematics as opposed to merely be using it?

The key observation is this: <mark>we have been doing mathematics for thousands of years before proofs came along</mark>. Mathematics is thus a fundamental human activity. We were here first, and so we do not owe it to academics that we are able to think in mathematical ways.

But what does “thinking in mathematical ways” even mean?

## II.

Let’s start from the end. Mazur identifies four intuitions that broadly cover all the various activities in current mathematical practice. First: **physical intuition**.

**Q**: _Woah there, isn’t physics qualitatively different from math? We verify physical truths by conducting experiments, not by proving things!_

Yeah, that’s a reasonable position to take. But insofar as you are able to predict in advance what would happen if you pushed a cup off a table WITHOUT any formal derivation whatsoever, you were able to arrive at (mostly) correct physical laws from an unbelievably finite set of experiences when you were just a toddler. Just imagine how powerful a distillation of information that was. Without much of a fuss, you went from _I can move_ -> _things move_ -> _I can move things_ -> _I am a thing that can be moved_ -> _I can move my legs to move my body upward_ ->  _I can lean forward to move my body downward_ -> _I can catch myself with my legs to move forward instead of downward_ -> _I can walk_ and so on.

This unconscious capacity to understand what it’s like to be an _embodied_ intelligence, to understand what you _can_ and _cannot_ do in terms of your own body and tools as its natural extensions, I think this is what physical intuition is all about. If I move A, B will happen. If I make C smaller, D will happen. Causation as capability. Intervention as a primitive action.

This kind of stuff is clearest from one of its greatest wielders. In a letter to Jacques Hadamard, Einstein said:

>   The words of the language, as they are written or spoken, do not seem to play any role in my mechanism of thought. The psychical entities which seem to serve as elements in thought are certain signs and more or less clear images which can be 'voluntarily' reproduced and combined.... The above mentioned elements are, in my case, of visual and some of a muscular type.

Visual imagination isn’t immediately math. It’s when you combine it in certain ways (which I claim is mediated by this physical intuition thing) that it begins to resemble something systematic and dare I say it, mathematical[^1]. Indeed, the thing that convinced me that this whole intuitions thing has at least some nontrivial amount of explanatory power is when I saw what happens when you _don’t_ have it. I posed the following question to my math major friends:

[^1]: Hell, if you’re still not convinced, try this on for size: when Einstein was grappling with the beginnings of special relativity, [he imagined what it would be like to ride along a wave of light](https://www.nytimes.com/2015/11/01/opinion/sunday/the-light-beam-rider.html).

>   You are presented with a three-legged stool on a flat surface. On which part of the seat’s edge must you push down for the stool to fall with the least amount of effort?

To my great surprise, a nontrivial number of them answered “above one of the legs…?”

![](http://cliparts101.com/files/319/EA75AE38A0F7C4AB6ADFEF53CE29781F/Wooden_Stool.png)

<small>Fig. 4: Here is a picture of a stool.</small>

Another intuition that physicists tend to possess in copious amounts is **computational intuition**. To me, this is the ability to anticipate the end result of a process. A person who has this in spades will excel in any area of math where s/he has to slog through a lot of calculations in order to get a result. Indeed, I think this is what we usually identify as the source of ‘genius’ in mathematics. The most famous mathematicians of old were all very computationally excellent: Archimedes, Gauss, Euler, Feynman[^2], von Neumann, etc.

[^2]: Stephen Wolfram claims that Feynman was [really, really good at calculating things](https://www.stephenwolfram.com/publications/short-talk-about-richard-feynman/). According to him, the reason Feynman got his larger-than-life reputation for his physical intuition was NOT because he made these huge leaps over problems like Einstein did, but because he did the long, hard calculations first and then only thought of a nice-sounding intuition pump shortcut afterwards. Take this with a grain of salt, however, because it seems Wolfram has always had a chip on his shoulder about him.

Mazur further claims that these people would excel in fields such as “machine computation, algorithms, numerical analysis, estimation, and statistics, to combinatorics, analysis and probability; i.e., ways of dealing with data, practically and/or theoretically.“

I…don’t have much else to say about this except that I personally don’t have much of it. I think my experience jives with a lot of mathematically traumatised people in this subreddit, because computational intuition is more or less the only thing you are trained to develop in K-12 education. Except for a brief stint with Euclid near the end of high school, no one really gets to (what I think is) the good stuff before uni.

And what is that good stuff? The third intuition in our toolkit is **algebraic intuition**. Sensitivity to structure, the drive to ever dizzying heights of abstraction. The main differentiator between its wielders is the _naturalness_ of their definitions. Indeed, the mathematician most graced with this trait is none other than Alexander Grothendieck. He was for many people (including myself) one of the greatest mathematicians of the last century and he exemplified this by championing a heuristic called the _relative point of view_. Imagine a slew of mathematical objects whose connections to each other are represented by arrows. Very roughly speaking, taking the relative POV means studying not the mathematical objects of such a network, but the _arrows_ instead. 

As an illustrative example, Allyn Jackson [gives us this apocryphal story](http://www.ams.org/notices/200410/fea-grothendieck-part2.pdf "WARNING: PDF"):

>   One striking characteristic of Grothendieck’s mode of thinking is that it seemed to rely so little
>   on examples. This can be seen in the legend of the so-called “Grothendieck prime”. In a mathematical conversation, someone suggested to Grothendieck that they should consider a particular prime number. “You mean an actual number?” Grothendieck asked. The other person replied, yes, an actual prime number. Grothendieck suggested, “All right, take 57.”
>
>   But Grothendieck must have known that 57 is not prime, right? Absolutely not, said David Mumford of Brown University. “He doesn’t think concretely.” Consider by contrast the Indian mathematician Ramanujan, who was intimately familiar with properties of many numbers, some of them huge. That way of thinking represents a world antipodal to that of Grothendieck. “He really never worked on examples,” Mumford observed. “I only understand things through examples and then gradually make them more abstract. I don’t think it helped Grothendieck in the least to look at an example. He really got control of the situation by thinking of it in absolutely the most abstract possible way. 

So how the hell does one not think of an example? Isn’t this a failure of rationality, complete antithesis to the [virtue of narrowness](http://yudkowsky.net/rational/virtues/)? Well, not really, because the relative point of view constrains a mathematical object via its relations to others so well that it is more or less completely determined by them. To Grothendieck, a prime must have meant something closer to "an element which cannot be written as a product of two other elements" or "the number of elements in a [cyclic group](https://en.wikipedia.org/wiki/Lagrange%27s_theorem_(group_theory))” rather than “2, 3, 5, and so on”. An elegant structure leaves no part to chance, and what can be narrower than that? 

(Indeed, I think this is what Yud was getting at when he was [trying to explain how Einstein arrived at general relativity by doing a Bayesian update in hypothesis-space rather than evidence-space](https://www.lesswrong.com/posts/MwQRucYo6BZZwjKE7/einstein-s-arrogance). That is, he cheated by updating on the structure of other successful theories in physics (in particular, Maxwell’s formulation of electromagnetism) instead of updating directly on the experimental evidence available at the time.)

Lastly, we have **geometric intuition**. I understand this as the propensity to build up from simpler notions. The first example that comes to mind is, of course, Euclid. From five postulates he and his students bedazzled millions of people for over 2000 years with increasingly complicated constructions like this bad boi over here:

[IMAGE](https://upload.wikimedia.org/wikipedia/commons/7/7a/EulerCircle4.gif)

_Fig. 5: The nine-point circle (in green). I’m sorry. I needed a break from all this text._

Under this paradigm, I claim that David Hilbert of the “axiomatise everything!” fame is one of the greatest geometric thinkers ever. Engineers must have this in spades as well; indeed, if we are to believe [Tesla](https://www.teslasautobiography.com/my_early_life.html) himself, he starts from the “great underlying principle” of a machine first before making modifications instead of what he claims others do, which is to engross themselves in the minutiae and build up towards the machine in question.

## III.

**Q**: Isn’t this all mumbo-jumbo? Just another personality test to be chucked in the bin after being debunked?

Perhaps. I’d be the first to say that it isn’t reductive enough. It doesn’t explain the geography of mathematics as it is practiced today, though I suspect it would explain the clustering of specific kinds of people in the _original_ incarnations of their corresponding subjects.

Indeed, Mazur notes this issue as well:

>   I say that these are “overlapping,’ but in reality, what makes mathematics one subject is that nothing that we do is entirely contained in one of these categories: they seem to stand for distinct intuitions, and have given rise to distinct realms of thought, and yet they are inseparably welded together. These four categories have been fused together so substantially in recent times that it may even be misleading to keep bringing them up. For there are subjects of great importance such as the theory of modular (or automorphic) forms that defy this categorization completely, since they stretch their substance, their tools, and the intuition they rely on over all four of these items.
>
>   Nevertheless, Geometry, Algebra, Computation, and Mathematical Physics represent a recognizable, if wobbly, partition of mathematical sensibilities. Let us consider them as motivating intuitions rather than fixed repositories of knowledge; i.e., as fundamental types of highly developed senses that some mathematicians enjoy […] I also think that we all understand what it means for some mathematician to have any one of these gifts—whether or not we ourselves possess it.

That last point is very important: geometry doesn’t mean thinking geometrically. Algebra doesn’t mean thinking algebraically. Problem may have solutions wildly different from the intuitions that asked them.

Now, where was I going with all this?

I was a math addict who got sober. From middle school until a couple of months past my teens, I imbibed math books. I taught myself proof-based real analysis, bought a compass and straightedge to reconstruct Euclid, and dragged myself through the pain of vector and tensor calculus to learn electrodynamics. Was I always this way? No. I wasn’t the best, the brightest, fastest kid in class when the symbols showed up. But math was in fashion in my academically competitive school, and God knows I'd never let myself get left behind.

In the back of my head, though, I knew. I just knew that it was all a sham. That I liked to read novels and encyclopedias more than I liked doing problems. That I had to work twice, thrice as hard as my peers just to reach their level of skill, and when that strategy didn’t work (which happened exceedingly often) I went off the beaten path and assumed mathematical hipsterism by _looking for the most sophisticated sounding math concept and grinding the shit out of it before everyone else_. Hell, in the early days of the ratsphere, [mathematical ability](https://www.lesswrong.com/posts/Rjw4qhEMvqskhL6Gm/beautiful-math) seemed the sine qua non of the Art, this thing that you have _got_ to be good at or else you’re a poo-flinging bottom-scraper who probably can’t use Bayes’ Theorem if your life depended on it. [Or if your daughter’s life depended on it](https://www.lesswrong.com/posts/SGR4GxFK7KmW7ckCB/something-to-protect). 

However, I was and always will be a Verbal. If aptitude tests are of any indication (and I hope there are still biodeterminists here), my raw mathematical ability lags behind my verbal ability by almost one-and-a-quarter SD. Calculations that people seemed to get instantly by some sleight of hand, I have to explicitly sound out step-by-step. And all this, it took me seven years and almost flunking out of college twice to admit to myself. 

So…I guess you could say this is my attempt at restitution. If math is such a fundamental part of the human experience, why should only a select few enjoy it in full? And it turns out the correct answer does NOT involve sacrificing goats to four-armed Hindu goddesses and just needs some capacity to be precise with words and simple diagrams[^3].

[^3]: Something that I'm going to take for granted everyone in this subreddit has. If you think you don't, read Jonah Sinick's [take on it on LessWrong](https://www.lesswrong.com/posts/AXXaXJvf7WcTessog/the-truth-about-mathematical-ability).