---
title: On keeping things
status: unfinished
confidence: highly likely
tags: narcissism
---

I have a wiki.

Yes, the same kind you used in high school to populate the bibliography section of your paper on 17th-century perfumery. But mine's about -- well, me -- and it saves me a lot of time and effort keeping track of all the things I want to do, all the games I want to experience or the things I want to pawn off some store's grubby shelf. We make a lot of bits just moving around by ourselves everyday and it's a darn shame we don't use that data as much as it should be.

Your life has more regularity in it than you think. Think about this: how do you brush your teeth? Do you brush your front teeth first or your molars? *When was the last time you shuffled that order?* Humans are creatures of routine but that isn't necessarily a bad thing. The way you learn things is that an activity naturally has to take up space in your short-term memory the first few times you do it. You have to be aware of the little things, the motion of your fingers, the flow of the argument. But after doing it a few times, something called "automaticity" takes over and things become less involved. At this point you are now able to talk a bit while driving or tell a joke while playing the horrendous part of Chopin's Minute Waltz[^1].

[^1]: Not a pianist but Chopin is famous for being difficult.

I'm a huge fan of spreadsheets so I pour as much of my life into them as possible. In my wiki I have spreadsheets for the following:

* games I need to play before I die
* films and series I need to see in order to become a Cultured Citizen
* textbooks/monographs written by smart people, usually dead ones
* books about building societies or destroying them

...and a few things that ought not be seen by the public. In fact, I'd wager the spreadsheet-to-page ratio of my wiki is very close to one.

Habits, wants, principles -- all these regularities are points on our map of ourselves. And therein lies the advantage of having your life on paper: you can see your life from above. You can plan routes, mark dangerous spots, and if need be tear off a few parts to make way for new territory. Collecting data about oneself and splaying it out on a table (or in my case on scriptable, 21st-century ones) is the modern fulfillment of Socrates' examined life[^2].

[^2]: Don't take my word on this. Take [Wikiquote's](https://en.wikiquote.org/wiki/Socrates).

But something's amiss about my wiki. Very early on, I made the decision of using Google Sheets to power my hard-on for tabulation. This was a matter of convenience over posterity: it's almost never a good idea to introduce external dependencies on long-term projects. And so I worry that when Don't Be Evil falls, my entire map will slide off the table.

### :::

Jason Scott is a modern-day hero. On the 15th of August 2015, he drove 230 miles to a warehouse to save 25 000 manuals[^3] from the dump. In a world where more information is generated every second than can be read in one's lifetime [citation needed], posterity is a mere afterthought. The firehose of information that violates our tiny mouths every day is hard enough to deal with, so it's not a surprise that only very few people are able to think about where and how to keep it.

[^3]: [This post](http://ascii.textfiles.com/archives/4683), from the man himself.

Going completely digital is a black hole waiting to happen. Electrons can only stay in the bits of hard drives for so long and, in the flash memory that is trying to replace them, even shorter. And physical decay is not the only problem. We put our data in fragile boxes, file formats that seldom describe themselves[^4]. If everyone suddenly dropped dead right now, the (decidedly non-human) archaeologists of tomorrow would be flabbergasted of the civilisation that suddenly started talking in code around the same time it invented plastic.

[^4]: There has been some progress on this front in the form of PDF/A, NetCDF, and HDF5 (sans how monolithic it appears).

I mentioned Jason Scott because a similar bomb dropped near me and I wasn't able to stop it. As a part of the oldest academic organisation in University X, I had a harrowing experience trying -- and failing -- to save a couple of important documents from the shredder. You see, the problem with all student-run organisations I know is that they're always a few years away from the gutter. Three or four years isn't enough to pass on all the aspects of a culture. Unless its founders had enough foresight to put a system in place to ensure its complete transmission, it is bound to get lost, decay or otherwise morph into an unrecognisable entropic mess by the time they invite you to their homecomings.

One aspect of Organisation X that has survived the second law of thermodynamics is the keeping of almost-verbatim meeting notes in log books. It's the job of the secretary to record every nook and cranny of discussions during assemblies. We had a cabinet of such records up to the 80's and it contained, amongst various doodles and runaway calculations, a record of who votes what on which issues. And what can you use that information for? That's right. Making lists of your members throughout the years.

So what happened to those logbooks? Well, every first Monday of the month, Organisation X requires its members to reduce the entropy of its club room. In University X, Mondays mean lab class so I had to fidget in a dark room for three hours straight while they were forming dead skin cells into mounds. An observation was made that the cabinet used by the people who get to decide on matters of membership was almost full due in great part to the presence of a couple of cough-inducing notebooks. A quick round of discussion later and those notebooks were put on a bus to the wonderful land of Your Local Scrapyard.

I had plans for those notebooks. I was going to hack a makeshift scanner and learn how to digitise an analog corpus. It would have been a fun project for the school break. *But no.* I must have made a few CO2 particles near-luminal because I heard loud and angry explosions behind me. When I arrived at the scrapyard, it was too late: the shredder had won and the annals of Organisation X was headed to the mills to become some sweaty old man's porn mag.

------

### Definitions

**automaticity**

: the phenomenon where doing something repeatedly makes you have to think less in order to do it

**Cultured Citizen**

: you know what this means

**Don't Be Evil**

: Google's official motto

**Jason Scott**

: archivist who's part of the venerable [archive.org](https://archive.org)

**Organisation X**

: the only school organisation I legitimately joined in college

**University X**

: the university I went to