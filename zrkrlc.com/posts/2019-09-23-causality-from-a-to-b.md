---
title: Causality from A to B
status: in progress
confidence: highly likely
tags: mathematics, rationality
---

### String diagrams

It is often said that everything in existence is connected by a grand _tapestry_ [?]. So to understand the universe, we must learn how to weave.

Consider a collection of _strings_ and _beads_.

[insert drawings of strings (denoted by arrows) and beads (denoted by boxes) here]

A wire can exist by itself, or it can be changed by being strung through a bead.

[side-by-side: identity and morphism]

You can string multiple beads together by arranging them in a chain (which we\'ll call the **composition operation**).

[composition]

> Meditation #1: Can a bead be strung by a wire to itself?

.

.

.

Answer: Yes. We can imagine a special bead, called the _identity bead_, that does nothing to a wire.

[-A-> [id] -A->  which is the same as -A->-o->-A> which is the same as -A->]

So whenever we encounter an identity bead, we can replace it with a wire carrying whatever its input was supposed to be.

Any and all such arrangements of beads and wires are called **string diagrams**, and these wires and beads and the composition operation, when taken together as primitives of a universe, forms what is known as a **category**. And immediately we can see that two diagrams are essentially the same as long as they preserve the connections between beads and the order in which they are strung.

[version of -> f_1 -> f_2 -> … -> f_n -> with kinks vs version without]

Now this universe is too simple. Surely we cannot make a tapestry [?] with just one chain of wires? So let\'s augment our universe by introducing a new operation: placing wires side-by-side (a.k.a. the **tensor product operation**):

```[ 
-T->
-S->
```



And since our wires were plucked from the idealised Platonic realm where all horrible entities await reckoning, let\'s make it so that we can string through a bead an arbitrary number of wires.

```[
-A_n-> [ ] -B_m->
-...-> [f] -...->
-A_1-> [ ] -B_1->
```

Note that the number of input wires may not be the same as the number of output ones.  Furthermore, _the order in which we place the wires matter_, so that:

```
-B->  =/= -A->
-A->      -B->
and
-B-> [ f ] -C-> is not the same as -A-> [ f\' ] -C\'->
-A-> [ f ]                         -B->
```

In this augmented universe, called the **monoidal category**, it doesn\'t matter how you move the wires and boxes around as long as you preserve the connections along each wire (like in the earlier **category** universe) and as long as you don\'t let the boxes and wires cross each other.

> Meditation # 2: Which of these diagrams are the same?

```
-B------>[g]---D->  =?= -B-[g]---------D->
-A-[f]---------C->      -A------>[f]---C->

[ ]---B-->[ ]    =?=   [ ]---B-->[ ]
[f]  [h]  [g]          [f]-->A-->[g]
[ ]-->A-->[ ]               [h]
```

.

.

.

Answer: Only the first equality holds. The second diagram breaks the no-crossing-of-wires-and-boxes rule.

Okay, now we\'re getting somewhere. If we feel that the no-crossing rule is too restrictive, we can augment our monoidal category two times. First, by allowing boxes to cross wires (which we\'ll call the **spacial monoidal category**):

```
 [h]   =  --A-->
--A-->     [h]
```

Second, by allowing the wires to cross using a new operation called _braiding_:

```
-B--\  /-A->
     \
-A--/ \--B->
```

In this second universe which we\'ll call the **braided monoidal category**, _which wire is on top of which matters_. So for instance:

```
-B--\  /-A-\ /-B->   =  --B-->
     \      /
-A--/ \--B-/ \-B->      --A-->

but

-B--\  /-A-\ /-B->   =/=  --B-->
     \      \
-A--/ \--B-/ \-B->        --A-->
```

> Meditation #3: Does the following equality hold in this universe?

```
-C-------C-\ /---A------A->     -C-\ /-B------B--\  /--A->
            \                       \             \
-B--\  /-A-/ \---C-\ /--B-> =?= -B-/ \--C-\  /-A-/ \---B->
     \              \                      \
-A--/ \--B-------B-/ \--C->     -A------A-/ \-C--------C->
```

.

.

.

Answer: Yes. One way of checking this is to walk each string as a path and count the _overs_ and _unders_ corresponding to the braiding under consideration. For instance, you\'ll notice that walking the C wire nets you two overs on both diagrams, while walking the B wire nets you one over and one under. Is it guaranteed that you\'ll always get the same ordering of the wires at the end if you follow the overs and unders of each wire?

To fully setup our causal universe, we need one more augmentation: symmetry.

```-B--\  /-A->
-B--\ /-A->
     X
-A--/ \--B->
```

That is, in this **symmetric monoidal category**, we ignore all the braids and simply allow wires to fully cross one another.

### The Category _Stoch_

Here\'s the kicker: we have enough structure in a symmetric monoidal category to talk about _probabilities_. Shown below is a translation table:

| **Our \"tapestry\"**       | Symmetric monoidal category | The category _Stoch_                 |
| ------------------------ | --------------------------- | ------------------------------------ |
| wires                    | (mathematical) objects      | finite sets                          |
| beads                    | maps/morphisms              | \$n \\times m\$ stochastic matrices [1] |
| composition operation    | composition                 | matrix multiplication [2]            |
| tensor product operation | tensor product              | Kronecker product [3]                |

----

[1]: Stochastic matrices are matrices of nonnegative whose columns sum to 1. An example:

```
[[0.2  0.4]
 [0.7  0.6]
 [0.1    0]]
```

The dimensions of the matrix is determined by the dimensions of the finite sets it is connecting. For instance, given the string diagram:

```
-A->[f]-B->
```

where A is the set {apple, pear, orange} of \$m = 3\$ elements and B is the set {ripe, unripe} of \$n = 2\$ elements, then a possible stochastic matrix f would be:

```
[[0.1 0.8 0.2]
 [0.9 0.2 0.8]]
```

which is a 2 x 3 matrix. In general, stochastic matrices are useful because each entry can be interpreted as a conditional probability, for example the matrix above can be taken to mean:

```
[[P(ripe|apple)   P(ripe|pear)   P(ripe|orange)]
 [P(unripe|apple) P(unripe|pear) P(unripe|orange)]]
```

where \$P(B|A)\$ is read as \"the probability of B given A\". Astute Bayesians would note that, for instance, \$P(ripe|apple) + P(unripe|apple) = 1\$ if a fruit is never both ripe and unripe simultaneously. 

[2]: Technically, it\'s matrix multiplication _then_ [marginalisation](https://en.wikipedia.org/wiki/Marginal_distribution) over the common variable but for our purposes you don\'t need to remember this.

[3]: A Kronecker product is different from the usual matrix multiplication. If we denote it by the symbol \$\otimes\$, then for example:

[example from Wikipedia]

When used with stochastic matrices, this product corresponds to taking the [product distribution](https://en.wikipedia.org/wiki/Product_distribution) of the distributions being multiplied.

----

(You don\'t need to think about the underlying numbers too much. Just think that beads carry probability distributions and wires are the entities we are trying to build a causal model of.)

Now, in _Stoch_ we identify two special kinds of stochastic matrices. A stochastic matrix which is just a column vector is called a **state** and are just priors of some variable:

```
[[0.1]  may be [[P(ripe)
 [0.9]]         [P(unripe)]]
```

In string diagrams, we write such matrices as beads without input:

```
[ ]-B->
[f]
[ ]-A->
```

> Meditation #4: The other special kind of stochastic matrix is a row vector representing what\'s known as **discarding map** (remember that beads correspond to maps?) denoted by –o. What are the entries of such a matrix?

.

.

.

Answer: Just 1\'s.  This map will play a special role our diagrams later.

An important property of _**Stoch**_ is that _only those wires which make it to the end correspond to observed variables_. For example, suppose we have the following:

A: 
B:
C: getting good 

The most straightforward causal model we can propose is represented by the string diagram:

```
[a]-A------[c]-C->
        / 
[b]-B--
```

 [TODO: explicate observed vs latent]

To fully build our category _**Stoch**_, we start from a symmetric monoidal category, then add three kinds of maps: the discarding map, and two other maps defined as the **copy map** and the **uniform state** [4] respectively:

```
      -A->
    /
-A-o
    \
      -A-> 
```

and

```
<|-o
```

[4]: So named because it corresponds to the stochastic matrix of uniform distribution. That is, if A is the set {apple, pear, orange} then its uniform state is a column vector whose entries are all equal to 1/3.

These maps must satisfy the following properties (omitting the labels this time):

```
            -->             -------->
          /               /
      --o         ==  --o         -->
    /     \               \     /
--o         -->             --o
    \                           \
      -------->                   -->
```

```
--o---->   == ------->
   \
     --o
```

```
      ---       -->              -->
    /     \   /                /
--o         X          ==  --o
    \     /   \                \
      ---       -->              -->
```

```
<|--o == (empty)
```

In particular, the first rule makes it possible to abuse our notation and draw arbitrarily many output copies from a single copy map.

By playing around with our rules, you can prove that:

```
-A->[f]-B-o == -B-o
```

for any set A and matrix f. In other words, sending wires to the discard map _discards_ all information that was hitherto being carried, hence the name.

Another thing you can show is the following property called **disintegration**. For any two-output state \$w\$ (representing a joint distribution) whose stochastic matrix has no zero entries (i.e., is said to have **full support**), there are always unique matrices \$a\$ and \$b\$ where:

```
[w]-A->  ==  [a] -o---A------A->
[ ]-B->            \
                     -A->[b]-B->
```

In other words, a fully supported joint distribution \"disintegrates\" into two separate stochastic matrices connected in this particular way.

## Combs

Is _**Stoch**_ enough to recover causality? Unfortunately, no. To see this, let\'s examine a classic example in the causal inference literature: the link between smoking and lung cancer.

A joint distribution between smoking (S) and lung cancer (C) is represented by the state \$w\$. A scientist claiming a causal link proposes the following model:

```
[w]-S->
[ ]-C->
```

We know from the previous section that this state disintegrates into the following string diagram:

```
[s]--o------S->
      \-[c]-C->
```

Interpreting this diagram as a causal model, we see that the scientist\'s claim is equivalent to scenario where there is a prior probability [s] for people to smoke and some intermediate process [c] that turns some of those smokers into people with lung cancer.  